<?php
include("./server/app.php");
$db = dbConexion();

$query = mysqli_query($db, "SELECT * FROM pizza");
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Pizzeria Pandemic</title>
		<?php include("./components/static.php"); ?>
		<!-- javascript -->
		<script src="<?php echo getLink(''); ?>app.js"></script>
		<!-- css -->
		<link href="<?php echo getLink(''); ?>assets/css/home.css" rel="stylesheet"/>

	</head>
	<body>
		<!-- navbar -->
		<nav class="navbar--main">
			<ul class="regular">
				<span class="logo">Pandemic</span>
				<?php if(usuarioActual()) : ?>
					<li class="li"><a href="#"><?php echo usuarioActual(); ?></a></li>
					<li class="li li--salir">
						<a href="./components/update_account_form.php">Update Account</a>
					</li>
					<li class="li li--salir">
						<a href="./components/salir.php">Salir</a>
					</li>
				<?php else: ?>
					<li class="li">
						<a href="./components/signin.html">Registrate</a>
					</li>
					<li class="li">
						<a href="./components/signup.html">Ingresar</a>
					</li>
				<?php endif; ?>
				<li class="li li--car">
					<a href="<?php echo getLink('components/carrito_detalle.php'); ?>">
						<i class="car fas fa-cart-plus text-danger"></i>
						<span class="car--count cart-items-count">0</span>
					</a>
				</li>
			</ul>
			<div class="slogan text-center">
				<h1>PIZZAS YA!</h1>
				<h3>Las mejores pizzas al instante</h3>
			</div>
		</nav>
		<div class="pasos pt-5 pb-5 text-center text-white">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<p class="text-center"><img src="https://img.pystatic.com/home-steps/step-1-food.png" class="content_steps_img" href=""></p>
						<h4>1. Elige tu comida</h4>
						<p>Mas de 10.000 Pizzeria Pandemic con delivery online.</p>
					</div>
					<div class="col-md-4">
						<p class="text-center"><img src="https://img.pystatic.com/home-steps/step-2-payment.png" class="content_steps_img" href=""></p>
						<h4>2. Realiza tu pedido</h4>
						<p>Es facil y rapido. Podras pagar online o en la entrega.</span>
					</div>
					<div class="col-md-4">
						<p class="text-center"><img src="https://img.pystatic.com/home-steps/step-3-delivery.png" class="content_steps_img" href=""></p>
						<h4>3. Recibe tu comida</h4>
						<p>La Pizzeria Pandemic entrega el pedido en tu puerta.</p>
					</div>
				</div>
			</div>
		</div>
		<!-- dashboard -->
		<div class="container--home">
			<?php	while($row = mysqli_fetch_row($query)): ?>
				<div class="card">
					<img src="<?php echo getLink('').$row[3]; ?>" alt="">
					<h4> <?php echo $row[1]; ?></h4>
					<h6>Ingredientes</h6>
					<p> <?php echo $row[4]; ?></p>
					<p>s/<?php echo $row[2]; ?></p>
					<button
						id="car"
								type="button"
								class="btn cart-add"
								data-id="<?php echo $row[0]; ?>"
								data-label="<?php echo $row[1]; ?>"
								data-price="<?php echo $row[2]*100;  ?>"
								data-image="<?php echo getLink($row[3]); ?>"
					>
						<i class="fas fa-shopping-cart fa-2x"></i>
					</button>
				</div>
			<?php endwhile; ?>
		</div>
		<!-- footer -->
		<?php include("./components/footer.php"); ?>
		<!-- javascript -->
		<script>
		 $(function(){
			 Cart.initJQuery();
			 Cart.currency = 's/';
		 });
		</script>
	</body>
</html>
