INSERT INTO `pizza`(`nombrePizza`, `precioPizza`, `fotoPizza`, `ingredientesPizza`, `cantidadPizza`)
VALUES
('Hawaiana', 22, 'assets/images/hawaiana.jpg' , 'Jamon, Chile, Cebolla y Queso',4),
('Napolitana', 25, 'assets/images/napolitana.jpg' , 'Queso, tomate y aceitunas',4),
('Americana', 30, 'assets/images/americana.jpg' , 'Pizza, Queso, Peperoni y Cebolla',4),
('Marinara', 20,'assets/images/americana.jpg','160 g de Harinao,100 ml de agua templada,cafe de sal,aceite de oliva,levadura seca',4),
('Sfincione',35,'assets/images/sfincione.jpg',' 6 champiñones,Pollo desmenuzado,Queso,Tomillo,Oregano,Aceite de oliva',3),
('fugazza con Queso',28,'assets/images/fugazza.jpg','cebolla roja,cebolla dulce,sal,aceite de oliva,queso parmesano,oregano',4),
('Chicago Pizza',39,'assets/images/chicagopizza.jpg','Harina de maiz,aceite de oliva,tomareMozzarella,Peperoni,Sausage,Salamim',5),
('New York Pizza',60,'assets/images/newyork.jpg','Harina de trigo,Agua,Azucar,Aceite de oliva,Tomate,Queso Mozzarella,Pepperonis',3),
('Mexicana', 22, 'assets/images/mexicana.jpg','Carne picada, Cebolla y Rojadas de aguacate',2),
('Pizza de pescado', 35, 'assets/images/pescado.jpg','Queso, Bonito y Anchoas ',3),
('Minipizzas', 18, 'assets/images/minipizza.jpg','Bonito, Jamon Cocido, Setas y Queso',4),
('Caprichosa', 23, 'assets/images/caprichosa.jpg','Anchoas, Champiñones, Alcachofas y Queso Mozzarella',3),
('Carbonara', 25, 'assets/images/carbonara.jpg','Queso rallado, Champiñones y Queso Mozzarella',2),
('Verde', 20, 'assets/images/verde.jpg','Pesto de albahaca, Brocoli, Calabacin, Queso azul y Oregano fresco',3),
('Pizza casero de pollo', 25, 'assets/images/casero.jpg','Tomate, Barbacoa, Pechuga de pollo y Champiñones',4),
('Margarita',21,'assets/images/margarita.jpg','salsa de tomate, harina, mozzarella, aceite , sal',3),
('Calzone',18,'assets/images/pizza_calzone.jpg','tomates maduros,mozzarella fresca,pimiento rojo,jamon ibarico',1),
('Diavola',17,'assets/images/diavola.jpg','pimientos,Salsa de tomate,salami,Mozarella rallada,pimienta blanca,cayena molida',2),
('Funghi',20,'assets/images/funghi.jpg','tomate frito,jamon,champiñones,setas,queso mozzarella,sal,aceite de oliva,tomillo',2),
('Mozzarella',23,'assets/images/mozzarrella.jpg','Aceite,Salsa de tomate,Mozzarella,Aceitunas',1),
('Margarita', 25, 'assets/images/margarita.jpg','Albacaha, Aceite de Oliva y Mozzarella',3),
('Napolitana', 30, 'assets/images/napolitana.jpg','Queso, Perejil y Aceitunas Verdes ',5),
('Quesos', 20, 'assets/images/cuatroquesos.jpg','Queso Gruyete, Queso Roquefort y Queso Pasmesano',2),
('Barbacoa', 35, 'assets/images/barbacoa.jpg','Pollo, Carne Picada, Barbacoa y Tomate Frito',4),
('Funghi', 25, 'assets/images/funghi.jpg','Jamon cocido, Oregano y Champiñones Portabello',3),
('Primavera', 40, 'assets/images/primavera.jpg','Levadura Prensado, Morron asado, Huevo y Perejil picado',2);

INSERT INTO `deliveryman`(`deliveryManNombre`, `deliveryManEstado`)
VALUES ("Jimmy erick","activo")


INSERT INTO `usuario`(`nombreUsuario`, `apellidoUsuario`, `cargoUsuario`, `usernameUsuario`, `passwordUsuario`)
VALUES
("Jimmy","Huamani Mendoza", "administrador","jimmy","jimmy")
