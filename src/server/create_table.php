<?php
$db = mysqli_connect("localhost", "root", "", "pizza");

$name_table = "cliente";
$query_sql = "create table `$name_table` (
`idCliente` int(6) not null AUTO_INCREMENT primary key,
 `nombreCliente` varchar(50) not null default '',
 `apellidoCliente` varchar(50) not null default '',
 `emailCliente` varchar(50) not null default '',
 `telefonoCliente` varchar(13) not null default '',
 `usernameCliente` varchar(80) not null default '',
 `passwordCliente` varchar(80) not null default ''
)";
mysqli_query($db, $query_sql);

$name_table_1 = "usuario";
$query_sql_1 = "create table `$name_table_1` (
`idUsuario` int(6) not null AUTO_INCREMENT primary key,
 `nombreUsuario` varchar(50) not null default '',
 `apellidoUsuario` varchar(50) not null default '',
 `cargoUsuario` varchar(50) not null default '',
 `usernameUsuario` varchar(80) not null default '',
 `passwordUsuario` varchar(80) not null default ''
)";
mysqli_query($db, $query_sql_1);

$name_table_4 = "deliveryMan";
$query_sql_4 = "create table `$name_table_4` (
`idDeliveryMan` int(6) not null AUTO_INCREMENT primary key,
 `deliveryManNombre` varchar(50) not null default '',
 `deliveryManEstado` varchar(10) not null default ''
)";
mysqli_query($db, $query_sql_4);

$name_table_3 = "pizza";
$query_sql_3 = "create table `$name_table_3` (
`idPizza` int(6) not null AUTO_INCREMENT primary key,
 `nombrePizza` varchar(50) not null default '',
 `precioPizza` float not null,
 `fotoPizza` varchar(250) not null,
 `ingredientesPizza` varchar(250) not null,
 `cantidadPizza` int(6) not null
)";
mysqli_query($db, $query_sql_3);

$name_table_2 = "pedido";
$query_sql_2 = "create table `$name_table_2` (
`idPedido` int(6) not null AUTO_INCREMENT primary key,
 `fechaPedido` datetime not null default current_timestamp,
 `fechaEntrega` varchar(50) not null default '',
 `horaEntrega` varchar(50) not null default '',
 `direccionPedido` varchar(100) not null default '',
 `idCliente` int ,
  foreign key (idCliente) references cliente(idCliente),
 `idDeliveryMan` int ,
  foreign key (idDeliveryMan) references deliveryMan(idDeliveryMan)
)";
mysqli_query($db, $query_sql_2);

$name_table_5 = "pedidoDetalle";
$query_sql_5 = "create table `$name_table_5` (
`idPedidoDetalle` int(6) not null AUTO_INCREMENT primary key,
 `idPedido` int not null,
  foreign key (idPedido) references pedido(idPedido),
 `idPizza` int not null,
  foreign key (idPizza) references pizza(idPizza),
  `cantidad` int not null,
 `precio` float not null
)";
mysqli_query($db, $query_sql_5);

mysqli_close($db);
?>
