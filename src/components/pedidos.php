<?php
include("../server/app.php");
if (!(isset($_SESSION['idUsuario']) and $_SESSION['idUsuario'])) {
  header("Location: ".getLink('')."components/usuario_login.html");
  exit();
}
$db = dbConexion();
$query = mysqli_query($db, "SELECT * FROM pedido P 
INNER JOIN cliente C ON C.idCliente = P.idCliente
INNER JOIN deliveryman DM ON DM.idDeliveryMan = P.idDeliveryMan");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Detalle venta</title>
		<?php include("./static.php"); ?>
  </head>
  <body>
		<div class="container">
			<h1 class="h1 text-center text-primary m-5">Lista de pedidos</h1>
			<table class="table table-striped text-center">
				<thead>
					<tr>
						<th align="center">Nro Pedido</th>
						<th align="center">Nombre cliente</th>
						<th align="center">Fecha pedido</th>
						<th align="center">Fecha entrega</th>
						<th align="center">Delivery man</th>
						<th align="center"></th>
					</tr>
				</thead>
				<tbody>
					<?php
					while($row = mysqli_fetch_object($query)):
					
					?>
						<tr>
							<td align="center">
								<?php echo $row->idPedido;?>
							</td>
							<td align="center">
								<?php echo $row->nombreCliente;?>
							</td>
							<td align="center">
								<?php echo $row->fechaPedido;?>
							</td>
							<td align="center">
								<?php echo $row->fechaEntrega . " ". $row->horaEntrega;?>
							</td>
							<td align="center">
								<?php echo $row->deliveryManNombre; ?>
							</td>
							<td align="center">
								<a href="pedido_detalle.php?idPedid=<?php echo $row->idPedido; ?>">Ver detalle</a>
							</td>
						</tr>
					<?php endwhile; ?>
				</tbody>
			</table>	
		</div>
		<p class="text-center">
			<a href="<?php echo getLink('components/salir.php'); ?>">Cerrar session</a>
		</p>
  </body>
</html>
