<footer class="footer text-center pb-3 pt-5 mt-5" >
  <div class="container bottom_border">
		<div class="row">
			<div class=" col-sm-4 col-md col-sm-4  col-12 col">
				<h5 class="headin5_amrc col_white_amrc pt2">Informacion</h5>
				<p class="mb10">La Linterna es un restaurante con mucha tradicion y recuerdos extrañables. Se inaugura en 1963, cuando su fundador, Floyd Scofield Sebastian, originario de White Plains, New York, abre su primer local en San Antonio, Miraflores, ofreciendo las clasicas pizzas neoyorquinas de crocante y delgada masa.</p>
				<p><i class="fa fa-location-arrow"></i> Av.Cultura 516</p>
				<p><i class="fa fa-phone"></i>  +51 983 456 456  </p>
				<p><i class="fa fa fa-envelope"></i> Luz@gmail.com</p>
			</div>
			<div class=" col-sm-4 col-md  col-6 col">
				<h5 class="headin5_amrc col_white_amrc pt2">Horario de Atencion</h5>
				<ul class="footer_ul_amrc">
					<li><b>Lunes a jueves :</b></li>
					<li><a>12:30-15:30 y 18:30-23:00</a></li>
					<li><b>Viernes y sabado :</b></li>
					<li><a>12:30-15:30 y 18:30-00:00</a></li>
					<li><b>Domingo :</b></li>
					<li><a>12:30-16:00 y 18:30-22:30</a></li>
				</ul>
			</div>
			<div class=" col-sm-4 col-md  col-6 col">
				<h5 class="headin5_amrc col_white_amrc pt2">REPARTO A DOMICILIO</h5>
				<ul class="footer_ul_amrc">
					<li><b>Lunes a jueves :</b></li>
					<li><a>12:30-15:30 y 18:30-23:00</a></li>
					<li><b>Viernes y sabado :</b></li>
					<li><a>12:30-15:30 y 18:30-00:00</a></li>
					<li><b>Domingo :</b></li>
					<li><a>12:30-16:00 y 18:30-22:30</a></li>
				</ul>
			</div>
			<div class=" col-sm-4 col-md  col-12 col">
				<h5 class="headin5_amrc col_white_amrc pt2">Nosotros</h5>
				<ul class="footer_ul2_amrc">
					<li><a href="#"><i class="fab fa-twitter fleft padding-right"></i> </a><p>Si quieres saber mas de nosotros visita nuestro twitter</p></li>
					<li><a href="#"><i class="fab fa-instagram fleft padding-right"></i> </a><p>Si quieres saber mas de nosotros visita nuestro instagram</p></li>
					<li><a href="#"><i class="fab fa-facebook fleft padding-right"></i> </a><p>Si quieres saber mas de nosotros visita nuestro facebook</p></li>
				</ul>
			</div>
		</div>
  </div>
  <div class="container">
		<ul class="social_footer_ul">
			<li><a href=""><i class="fab fa-facebook-f"></i></a></li>
			<li><a href=""><i class="fab fa-twitter"></i></a></li>
			<li><a href=""><i class="fab fa-linkedin"></i></a></li>
			<li><a href=""><i class="fab fa-instagram"></i></a></li>
    </ul>
		<p class="text-center">Copyright @2021 | Designed With by <a href="<?php echo getLink('components/usuario_login.html'); ?>">Pandemic</a></p>
  </div>
</footer>
